import * as React from "react";
// import { useState, useRef, useEffect } from "react";

function Navigation(props) {
  let activePage = props.activePage;

  const layersClick = () => {
    props.onPageSelection("layers");
  };

  const bulkListClick = () => {
    props.onPageSelection("bulk");
  };

  const layersByTypeClick = () => {
    props.onPageSelection("layers_by_type");
  };

  return (
    <div className="navigation-wrapper">
      <nav className="nav">
        <div
          className={`nav-item ${activePage === "layers" ? "active" : ""}`}
          onClick={layersClick}
        >
          Layers
        </div>
        <div
          className={`nav-item ${activePage === "bulk" ? "active" : ""}`}
          onClick={bulkListClick}
        >
          Errors by Count
        </div>
        <div
          className={`nav-item ${activePage === "layers_by_type" ? "active" : ""}`}
          onClick={layersByTypeClick}
        >
          Errors by Type
        </div>
      </nav>
    </div>
  );
}

export default Navigation;
